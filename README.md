# Beispiel Gitlab Pipeline für einen Microservice

Diese Gitlab Pipeline ist eine Beispiel Pipeline für einen Microservice.
Das Beispiel enthält verschiedene Pipileine Stages und Jobs, 
wie ich sie für einen Mikroservice einbauen würde.

Das Beispiel enthält keinen Microservice.

Das Beispiel geht davon aus, dass es ein darunterliegendes Projekt gibt, 
mit dem die einzelnen Jobs ausgeführt werden können.